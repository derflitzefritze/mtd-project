//
//  main.m
//  MTD-Foo
//
//  Created by Dirk Fritz on 10.11.14.
//  Copyright (c) 2014 Dirk Fritz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
