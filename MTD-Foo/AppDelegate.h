//
//  AppDelegate.h
//  MTD-Foo
//
//  Created by Dirk Fritz on 10.11.14.
//  Copyright (c) 2014 Dirk Fritz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

