//
//  ViewController.m
//  MTD-Foo
//
//  Created by Dirk Fritz on 10.11.14.
//  Copyright (c) 2014 Dirk Fritz. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
